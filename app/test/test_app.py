
def test_empty(client):
    request = client.post('/vowel_count', data='[]')
    assert b'{}' in request.data

def test_post_vowel_count(client):
    resquest = client.post('/vowel_count', data='["batman", "robin", "coringa"]')
    assert resquest.get_json()['batman'] == 2
    assert resquest.get_json()['robin'] == 2
    assert resquest.get_json()['coringa'] == 3

def test_post_vowel_count_uppercase(client):
    resquest = client.post('/vowel_count', data='["BANANA", "APPLE"]')
    assert resquest.get_json()['BANANA'] == 3
    assert resquest.get_json()['APPLE'] == 2