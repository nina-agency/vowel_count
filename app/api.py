from typing import Any
from venv import create
from flask import Flask, request, json, abort
from flask_restx import Api, Resource, fields, fields
from werkzeug.middleware.proxy_fix import ProxyFix
from werkzeug.exceptions import HTTPException, BadRequest
import logging

def create_app():
    app = Flask(__name__)
    app.wsgi_app = ProxyFix(app.wsgi_app)
    api = Api(app, version='1.0', title='Vowel Count API',
        description='An API to count vowels from a wordlist',
    )

    vowel_count = api.model('VowelCount', {
        'wordlist': fields.List(fields.String)
    })



    @api.route('/vowel_count')
    class VowelCount(Resource):
        
        @api.expect(vowel_count)
        def post(self):
            try:
                data = json.loads(request.get_data())
                data_vowel_count={}
                words = []
                if "wordlist" in data:
                    words = data["wordlist"]
                else:
                    words = data
                for item in words:
                    vowel_count = list(filter(lambda x: x in r'aeiou', item.lower()))
                    data_vowel_count[item] = len(vowel_count)

                return json.jsonify(data_vowel_count)
            except Exception as e:
                logging.exception('ex')
                return json.jsonify({"error":e.args})
    return app

if __name__ == '__main__':
    app = create_app()
    app.run(debug=True)